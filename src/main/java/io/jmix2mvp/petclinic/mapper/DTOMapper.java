package io.jmix2mvp.petclinic.mapper;

import io.jmix2mvp.petclinic.dto.*;
import io.jmix2mvp.petclinic.entity.*;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface DTOMapper {

    PetTypeDTO petTypeToDTO(PetType petType);

    PetTypeInputDTO petTypeToInputDTO(PetType petType);

    void petTypeDTOToEntity(PetTypeInputDTO dto, @MappingTarget PetType entity);

    PetDTO petToDTO(Pet pet);

    void petDTOToEntity(PetInputDTO dto, @MappingTarget Pet entity);

    OwnerDTO ownerToDTO(Owner owner);

    void ownerDTOToEntity(OwnerInputDTO dto, @MappingTarget Owner entity);

    VisitDTO visitToDTO(Visit visit);

    void visitDTOToEntity(VisitInputDTO dto, @MappingTarget Visit entity);

    TestDTO testEntityToDTO(TestEntity testEntity);

    void testEntityDTOToEntity(TestInputDTO dto, @MappingTarget TestEntity entity);

    Tag tagDTOToTag(TagDTO tagDto);

    TagDTO tagToTagDTO(Tag tag);

    void tagDTOToEntity(TagInputDTO dto, @MappingTarget Tag entity);

    @Mapping(target = "id", source = "identifier")
    PetDescriptionDTO petDescriptionToDTO(PetDescription petDescription);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "identifier", source = "id")
    void updatePetDescriptionFromPetDescriptionDTO(PetDescriptionDTO petDescriptionDTO, @MappingTarget PetDescription petDescription);

    @Mapping(target = "petDiseaseIdentifier", source = "id")
    PetDisease petDiseaseDTOToEntity(PetDiseaseDTO petDiseaseDTO);

    @Mapping(target = "id", source = "petDiseaseIdentifier")
    PetDiseaseDTO petDiseaseToPetDiseaseDTO(PetDisease petDisease);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "petDiseaseIdentifier", source = "id")
    void updatePetDiseaseFromPetDiseaseInputDTO(PetDiseaseInputDTO petDiseaseInputDTO, @MappingTarget PetDisease petDisease);

    @Mapping(target = "identifier", source = "id")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updatePetDescriptionFromPetDescriptionInputDTO(PetDescriptionInputDTO petDescriptionInputDTO, @MappingTarget PetDescription petDescription);

    Pet tinyDtoToPet(TransientPetDto transientPetDto);
}
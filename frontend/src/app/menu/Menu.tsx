import { Menu } from "antd";
import { ItemType } from "antd/es/menu/hooks/useItems";
import { HomeOutlined } from "@ant-design/icons";
import { Link, useLocation } from "react-router-dom";
import { FormattedMessage } from "react-intl";

const menuItems: ItemType[] = [
  {
    label: (
      <Link to={""}>
        <FormattedMessage id={"screen.home"} />
      </Link>
    ),
    key: "",
    icon: <HomeOutlined />
  },
  {
    label: (
      <Link to="blank-component">
        <FormattedMessage id="screen.BlankComponent" />
      </Link>
    ),
    key: "blank-component"
  },
  {
    label: (
      <Link to="owner-lookup-cards">
        <FormattedMessage id="screen.OwnerLookupCards" />
      </Link>
    ),
    key: "owner-lookup-cards"
  },
  {
    label: (
      <Link to="pet-lookup-cards">
        <FormattedMessage id="screen.PetLookupCards" />
      </Link>
    ),
    key: "pet-lookup-cards"
  },
  {
    label: (
      <Link to="pet-type-lookup-cards">
        <FormattedMessage id="screen.PetTypeLookupCards" />
      </Link>
    ),
    key: "pet-type-lookup-cards"
  },
  {
    label: (
      <Link to="pet-disease-lookup-cards">
        <FormattedMessage id="screen.PetDiseaseLookupCards" />
      </Link>
    ),
    key: "pet-disease-lookup-cards"
  },
  {
    label: (
      <Link to="scalars-lookup-cards">
        <FormattedMessage id="screen.ScalarsLookupCards" />
      </Link>
    ),
    key: "scalars-lookup-cards"
  },
  {
    label: (
      <Link to="scalars-not-null-lookup-cards">
        <FormattedMessage id="screen.ScalarsNotNullLookupCards" />
      </Link>
    ),
    key: "scalars-not-null-lookup-cards"
  },
  {
    label: (
      <Link to="pet-description-lookup-cards">
        <FormattedMessage id="screen.PetDescriptionLookupCards" />
      </Link>
    ),
    key: "pet-description-lookup-cards"
  },
  {
    label: (
      <Link to="owner-list">
        <FormattedMessage id="screen.OwnerList" />
      </Link>
    ),
    key: "owner-list"
  },
  {
    label: (
      <Link to="owner-cards">
        <FormattedMessage id="screen.OwnerCards" />
      </Link>
    ),
    key: "owner-cards"
  },
  {
    label: (
      <Link to="owner-table">
        <FormattedMessage id="screen.OwnerTable" />
      </Link>
    ),
    key: "owner-table"
  },
  {
    label: (
      <Link to="pet-list">
        <FormattedMessage id="screen.PetList" />
      </Link>
    ),
    key: "pet-list"
  },
  {
    label: (
      <Link to="pet-cards">
        <FormattedMessage id="screen.PetCards" />
      </Link>
    ),
    key: "pet-cards"
  },
  {
    label: (
      <Link to="pet-table">
        <FormattedMessage id="screen.PetTable" />
      </Link>
    ),
    key: "pet-table"
  },
  {
    label: (
      <Link to="scalars-list">
        <FormattedMessage id="screen.ScalarsList" />
      </Link>
    ),
    key: "scalars-list"
  },
  {
    label: (
      <Link to="scalars-table">
        <FormattedMessage id="screen.ScalarsTable" />
      </Link>
    ),
    key: "scalars-table"
  },
  {
    label: (
      <Link to="scalars-not-null-cards">
        <FormattedMessage id="screen.ScalarsNotNullCards" />
      </Link>
    ),
    key: "scalars-not-null-cards"
  },
  {
    label: (
      <Link to="pet-disease-list">
        <FormattedMessage id="screen.PetDiseaseList" />
      </Link>
    ),
    key: "pet-disease-list"
  },
  {
    label: (
      <Link to="pet-disease-cards">
        <FormattedMessage id="screen.PetDiseaseCards" />
      </Link>
    ),
    key: "pet-disease-cards"
  },
  {
    label: (
      <Link to="pet-disease-table">
        <FormattedMessage id="screen.PetDiseaseTable" />
      </Link>
    ),
    key: "pet-disease-table"
  },
  {
    label: (
      <Link to="pet-type-table">
        <FormattedMessage id="screen.PetTypeTable" />
      </Link>
    ),
    key: "pet-type-table"
  },
  {
    label: (
      <Link to="client-validation-test-entity-cards">
        <FormattedMessage id="screen.ClientValidationTestEntityCards" />
      </Link>
    ),
    key: "client-validation-test-entity-cards"
  },
  {
    label: (
      <Link to="read-only-owner-list">
        <FormattedMessage id="screen.ReadOnlyOwnerList" />
      </Link>
    ),
    key: "read-only-owner-list"
  },
  {
    label: (
      <Link to="read-only-owner-cards">
        <FormattedMessage id="screen.ReadOnlyOwnerCards" />
      </Link>
    ),
    key: "read-only-owner-cards"
  },
  {
    label: (
      <Link to="read-only-owner-table">
        <FormattedMessage id="screen.ReadOnlyOwnerTable" />
      </Link>
    ),
    key: "read-only-owner-table"
  },
  {
    label: (
      <Link to="read-only-pet-list">
        <FormattedMessage id="screen.ReadOnlyPetList" />
      </Link>
    ),
    key: "read-only-pet-list"
  },
  {
    label: (
      <Link to="read-only-pet-cards">
        <FormattedMessage id="screen.ReadOnlyPetCards" />
      </Link>
    ),
    key: "read-only-pet-cards"
  },
  {
    label: (
      <Link to="read-only-pet-table">
        <FormattedMessage id="screen.ReadOnlyPetTable" />
      </Link>
    ),
    key: "read-only-pet-table"
  },
  {
    label: (
      <Link to="read-only-scalars-list">
        <FormattedMessage id="screen.ReadOnlyScalarsList" />
      </Link>
    ),
    key: "read-only-scalars-list"
  },
  {
    label: (
      <Link to="read-only-pet-disease-list">
        <FormattedMessage id="screen.ReadOnlyPetDiseaseList" />
      </Link>
    ),
    key: "read-only-pet-disease-list"
  },
  {
    label: (
      <Link to="standalone-owner-list">
        <FormattedMessage id="screen.StandaloneOwnerList" />
      </Link>
    ),
    key: "standalone-owner-list"
  },
  {
    label: (
      <Link to="standalone-owner-cards">
        <FormattedMessage id="screen.StandaloneOwnerCards" />
      </Link>
    ),
    key: "standalone-owner-cards"
  },
  {
    label: (
      <Link to="standalone-owner-table">
        <FormattedMessage id="screen.StandaloneOwnerTable" />
      </Link>
    ),
    key: "standalone-owner-table"
  },
  {
    label: (
      <Link to="standalone-pet-disease-list">
        <FormattedMessage id="screen.StandalonePetDiseaseList" />
      </Link>
    ),
    key: "standalone-pet-disease-list"
  },
  {
    label: (
      <Link to="standalone-scalars-cards">
        <FormattedMessage id="screen.StandaloneScalarsCards" />
      </Link>
    ),
    key: "standalone-scalars-cards"
  },
  {
    label: (
      <Link to="standalone-read-only-pet-cards">
        <FormattedMessage id="screen.StandaloneReadOnlyPetCards" />
      </Link>
    ),
    key: "standalone-read-only-pet-cards"
  },
  {
    label: (
      <Link to="standalone-read-only-pet-list">
        <FormattedMessage id="screen.StandaloneReadOnlyPetList" />
      </Link>
    ),
    key: "standalone-read-only-pet-list"
  },
  {
    label: (
      <Link to="standalone-read-only-pet-table">
        <FormattedMessage id="screen.StandaloneReadOnlyPetTable" />
      </Link>
    ),
    key: "standalone-read-only-pet-table"
  },
  {
    label: (
      <Link to="owner-table-with-multiselect">
        <FormattedMessage id="screen.OwnerTableWithMultiselect" />
      </Link>
    ),
    key: "owner-table-with-multiselect"
  },
  {
    label: (
      <Link to="owner-cards-with-filter-sort-page">
        <FormattedMessage id="screen.OwnerCardsWithFilterSortPage" />
      </Link>
    ),
    key: "owner-cards-with-filter-sort-page"
  },
  {
    label: (
      <Link to="owner-cards-with-filter">
        <FormattedMessage id="screen.OwnerCardsWithFilter" />
      </Link>
    ),
    key: "owner-cards-with-filter"
  },
  {
    label: (
      <Link to="owner-cards-with-sort">
        <FormattedMessage id="screen.OwnerCardsWithSort" />
      </Link>
    ),
    key: "owner-cards-with-sort"
  },
  {
    label: (
      <Link to="owner-cards-with-page">
        <FormattedMessage id="screen.OwnerCardsWithPage" />
      </Link>
    ),
    key: "owner-cards-with-page"
  },
  {
    label: (
      <Link to="owner-lists-with-filter-sort-page">
        <FormattedMessage id="screen.OwnerListsWithFilterSortPage" />
      </Link>
    ),
    key: "owner-lists-with-filter-sort-page"
  },
  {
    label: (
      <Link to="owner-list-with-filter">
        <FormattedMessage id="screen.OwnerListWithFilter" />
      </Link>
    ),
    key: "owner-list-with-filter"
  },
  {
    label: (
      <Link to="owner-list-with-sort">
        <FormattedMessage id="screen.OwnerListWithSort" />
      </Link>
    ),
    key: "owner-list-with-sort"
  },
  {
    label: (
      <Link to="owner-list-with-page">
        <FormattedMessage id="screen.OwnerListWithPage" />
      </Link>
    ),
    key: "owner-list-with-page"
  },
  {
    label: (
      <Link to="owner-table-with-filter-sort-page">
        <FormattedMessage id="screen.OwnerTableWithFilterSortPage" />
      </Link>
    ),
    key: "owner-table-with-filter-sort-page"
  },
  {
    label: (
      <Link to="owner-table-with-filter">
        <FormattedMessage id="screen.OwnerTableWithFilter" />
      </Link>
    ),
    key: "owner-table-with-filter"
  },
  {
    label: (
      <Link to="owner-table-with-sort">
        <FormattedMessage id="screen.OwnerTableWithSort" />
      </Link>
    ),
    key: "owner-table-with-sort"
  },
  {
    label: (
      <Link to="owner-table-with-page">
        <FormattedMessage id="screen.OwnerTableWithPage" />
      </Link>
    ),
    key: "owner-table-with-page"
  },
  {
    label: (
      <Link to="scalars-not-null-cards-with-filter-sort-page">
        <FormattedMessage id="screen.ScalarsNotNullCardsWithFilterSortPage" />
      </Link>
    ),
    key: "scalars-not-null-cards-with-filter-sort-page"
  },
  {
    label: (
      <Link to="scalars-cards-with-filter-sort-page">
        <FormattedMessage id="screen.ScalarsCardsWithFilterSortPage" />
      </Link>
    ),
    key: "scalars-cards-with-filter-sort-page"
  },
  {
    label: (
      <Link to="visit-with-filter">
        <FormattedMessage id="screen.VisitWithFilter" />
      </Link>
    ),
    key: "visit-with-filter"
  },
  {
    label: (
      <Link to="read-only-owner-cards-with-filter-sort-page">
        <FormattedMessage id="screen.ReadOnlyOwnerCardsWithFilterSortPage" />
      </Link>
    ),
    key: "read-only-owner-cards-with-filter-sort-page"
  },
  {
    label: (
      <Link to="read-only-owner-list-with-filter-sort-page">
        <FormattedMessage id="screen.ReadOnlyOwnerListWithFilterSortPage" />
      </Link>
    ),
    key: "read-only-owner-list-with-filter-sort-page"
  },
  {
    label: (
      <Link to="read-only-owner-table-with-filter-sort-page">
        <FormattedMessage id="screen.ReadOnlyOwnerTableWithFilterSortPage" />
      </Link>
    ),
    key: "read-only-owner-table-with-filter-sort-page"
  },
  {
    label: (
      <Link to="standalone-read-only-owner-cards-with-filter-sort-page">
        <FormattedMessage id="screen.StandaloneReadOnlyOwnerCardsWithFilterSortPage" />
      </Link>
    ),
    key: "standalone-read-only-owner-cards-with-filter-sort-page"
  },
  {
    label: (
      <Link to="standalone-read-only-owner-list-with-filter-sort-page">
        <FormattedMessage id="screen.StandaloneReadOnlyOwnerListWithFilterSortPage" />
      </Link>
    ),
    key: "standalone-read-only-owner-list-with-filter-sort-page"
  },
  {
    label: (
      <Link to="standalone-read-only-owner-table-with-filter-sort-page">
        <FormattedMessage id="screen.StandaloneReadOnlyOwnerTableWithFilterSortPage" />
      </Link>
    ),
    key: "standalone-read-only-owner-table-with-filter-sort-page"
  },
  {
    label: (
      <Link to="owner-cards-with-result-page">
        <FormattedMessage id="screen.OwnerCardsWithResultPage" />
      </Link>
    ),
    key: "owner-cards-with-result-page"
  }
];

export const AppMenu = () => {
  const { pathname } = useLocation();
  const selectedKey = toSelectedKey(pathname);

  return <Menu selectedKeys={[selectedKey]} items={menuItems} />;
};

function toSelectedKey(pathname: string) {
  return pathname.split("/", 2).join("");
}

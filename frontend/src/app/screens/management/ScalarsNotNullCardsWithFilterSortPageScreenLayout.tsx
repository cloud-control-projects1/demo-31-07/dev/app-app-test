import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Breadcrumb } from "antd";
import { ScalarsNotNullCardsWithFilterSortPage } from "./ScalarsNotNullCardsWithFilterSortPage";
import { ScalarsNotNullCardsWithFilterSortPageEditor } from "./ScalarsNotNullCardsWithFilterSortPageEditor";
import { BreadcrumbContext } from "../../../core/screen/BreadcrumbContext";
import { usePageTitle } from "../../../core/screen/usePageTitle";
import { useIntl } from "react-intl";

export function ScalarsNotNullCardsWithFilterSortPageScreenLayout() {
  const intl = useIntl();
  usePageTitle(
    intl.formatMessage({ id: "screen.ScalarsNotNullCardsWithFilterSortPage" })
  );

  const { recordId } = useParams();
  const [breadcrumbItems, setBreadcrumbItems] = useState<string[]>([]);

  return (
    <>
      {recordId && (
        <Breadcrumb className="crud-screen-breadcrumb">
          {breadcrumbItems.map((item, index) => (
            <Breadcrumb.Item key={`breadcrumb${index}`}>{item}</Breadcrumb.Item>
          ))}
        </Breadcrumb>
      )}

      <BreadcrumbContext.Provider value={setBreadcrumbItems}>
        <div style={{ display: recordId ? "none" : "block" }}>
          <ScalarsNotNullCardsWithFilterSortPage />
        </div>
        {recordId && (
          <ScalarsNotNullCardsWithFilterSortPageEditor
            refetchQueries={[
              "Get_NN_Scalars_Test_Entity_List_With_Filter_Page_Sort"
            ]}
          />
        )}
      </BreadcrumbContext.Provider>
    </>
  );
}

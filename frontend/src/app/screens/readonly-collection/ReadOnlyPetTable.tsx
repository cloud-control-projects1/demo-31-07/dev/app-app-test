import { useMemo, useState } from "react";
import { useQuery } from "@apollo/client";
import { ApolloError } from "@apollo/client/errors";
import { ResultOf } from "@graphql-typed-document-node/core";
import { Button, Space, Table } from "antd";
import { useNavigate } from "react-router-dom";
import { FormattedMessage, useIntl } from "react-intl";
import { gql } from "@amplicode/gql";
import { RequestFailedError } from "../../../core/crud/RequestFailedError";
import { deserialize } from "../../../core/transform/model/deserialize";
import { getPetTypeDtoDisplayName } from "../../../core/display-name/getPetTypeDtoDisplayName";
import { getOwnerDtoDisplayName } from "../../../core/display-name/getOwnerDtoDisplayName";
import { getPetDescriptionDtoDisplayName } from "../../../core/display-name/getPetDescriptionDtoDisplayName";
import { getTagDtoDisplayName } from "../../../core/display-name/getTagDtoDisplayName";
import { getPetDiseaseDtoDisplayName } from "../../../core/display-name/getPetDiseaseDtoDisplayName";
import { useBreadcrumbItem } from "../../../core/screen/useBreadcrumbItem";

const PET_LIST = gql(`
  query Get_Pet_List {
    petList {
      id
      identificationNumber
      birthDate
      type {
        id
        name
      }
      owner {
        id
        firstName
        lastName
      }
      description {
        id
        description
      }
      tags {
        id
        name
      }
      diseases {
        id
        name
        description
      }
    }
  }
`);

const columns = [
  {
    title: "Identification Number",
    dataIndex: "identificationNumber",
    key: "identificationNumber"
  },
  {
    title: "Birth Date",
    dataIndex: "birthDate",
    key: "birthDate"
  },
  {
    title: "Type",
    dataIndex: "type",
    key: "type"
  },
  {
    title: "Owner",
    dataIndex: "owner",
    key: "owner"
  },
  {
    title: "Description",
    dataIndex: "description",
    key: "description"
  },
  {
    title: "Tags",
    dataIndex: "tags",
    key: "tags"
  },
  {
    title: "Diseases",
    dataIndex: "diseases",
    key: "diseases"
  }
];

export function ReadOnlyPetTable() {
  const intl = useIntl();
  useBreadcrumbItem(intl.formatMessage({ id: "screen.ReadOnlyPetTable" }));

  // Load the items from server
  const { loading, error, data } = useQuery(PET_LIST);

  const items = useMemo(() => deserialize(data?.petList), [data?.petList]);

  // selected row id
  const [selectedRowId, setSelectedRowId] = useState();

  return (
    <div className="narrow-layout">
      <Space direction="vertical" className="table-space">
        <ButtonPanel selectedRowId={selectedRowId} />
        <TableSection
          items={items}
          loading={loading}
          error={error}
          selectedRowId={selectedRowId}
          setSelectedRowId={setSelectedRowId}
        />
      </Space>
    </div>
  );
}

interface ButtonPanelProps {
  selectedRowId?: string;
}
/**
 * Button panel above
 */
function ButtonPanel({ selectedRowId }: ButtonPanelProps) {
  const intl = useIntl();
  const navigate = useNavigate();

  return (
    <Space direction="horizontal">
      <Button
        htmlType="button"
        key="viewDetails"
        title={intl.formatMessage({ id: "common.viewDetails" })}
        disabled={selectedRowId == null}
        onClick={() => selectedRowId && navigate(selectedRowId)}
      >
        <span>
          <FormattedMessage id="common.viewDetails" />
        </span>
      </Button>
    </Space>
  );
}

interface TableSectionProps {
  items?: ItemTableType;
  loading?: boolean;
  error?: ApolloError;
  selectedRowId?: string;
  setSelectedRowId: (id: any) => any;
}

/**
 * Collection of items
 */
function TableSection({
  items,
  loading,
  error,
  selectedRowId,
  setSelectedRowId
}: TableSectionProps) {
  if (error) {
    return <RequestFailedError />;
  }

  const dataSource = items
    ?.filter(item => item != null)
    .map(item => ({
      key: item?.id,
      ...item,
      ...{
        birthDate: item!.birthDate?.format("LL") ?? undefined,
        type: getPetTypeDtoDisplayName(item!.type ?? undefined),
        owner: getOwnerDtoDisplayName(item!.owner ?? undefined),
        description: getPetDescriptionDtoDisplayName(
          item!.description ?? undefined
        ),
        tags:
          item &&
          item.tags &&
          item.tags
            .map(entry => getTagDtoDisplayName(entry))
            .filter(entry => entry !== "")
            .join(", "),
        diseases:
          item &&
          item.diseases &&
          item.diseases
            .map(entry => getPetDiseaseDtoDisplayName(entry))
            .filter(entry => entry !== "")
            .join(", ")
      }
    }));

  return (
    <Space direction="vertical" className="table-space entity-table">
      <Table
        loading={loading}
        dataSource={dataSource}
        columns={columns}
        rowClassName={record =>
          (record as ItemType)?.id === selectedRowId ? "table-row-selected" : ""
        }
        onRow={data => {
          return {
            onClick: () => {
              const id = (data as ItemType)?.id;
              setSelectedRowId(id === selectedRowId ? null : id);
            }
          };
        }}
        scroll={{ x: true }}
        pagination={false}
      />
    </Space>
  );
}

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof PET_LIST>;
/**
 * Type of the items list
 */
type ItemTableType = QueryResultType["petList"];
/**
 * Type of a single item
 */
type ItemType = Exclude<ItemTableType, null | undefined>[0];

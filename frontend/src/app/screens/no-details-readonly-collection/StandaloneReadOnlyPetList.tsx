import { useMemo } from "react";
import { useQuery } from "@apollo/client";
import { ApolloError } from "@apollo/client/errors";
import { ResultOf } from "@graphql-typed-document-node/core";
import { Empty, List, Space, Spin } from "antd";
import { useIntl } from "react-intl";
import { gql } from "@amplicode/gql";
import { ValueWithLabel } from "../../../core/crud/ValueWithLabel";
import { RequestFailedError } from "../../../core/crud/RequestFailedError";
import { deserialize } from "../../../core/transform/model/deserialize";
import { getPetTypeDtoDisplayName } from "../../../core/display-name/getPetTypeDtoDisplayName";
import { getOwnerDtoDisplayName } from "../../../core/display-name/getOwnerDtoDisplayName";
import { getPetDescriptionDtoDisplayName } from "../../../core/display-name/getPetDescriptionDtoDisplayName";
import { getTagDtoDisplayName } from "../../../core/display-name/getTagDtoDisplayName";
import { getPetDiseaseDtoDisplayName } from "../../../core/display-name/getPetDiseaseDtoDisplayName";
import { useBreadcrumbItem } from "../../../core/screen/useBreadcrumbItem";

const PET_LIST = gql(`
  query Get_Pet_List {
    petList {
      id
      identificationNumber
      birthDate
      type {
        id
        name
      }
      owner {
        id
        firstName
        lastName
      }
      description {
        id
        description
      }
      tags {
        id
        name
      }
      diseases {
        id
        name
        description
      }
    }
  }
`);

export function StandaloneReadOnlyPetList() {
  const intl = useIntl();
  useBreadcrumbItem(
    intl.formatMessage({ id: "screen.StandaloneReadOnlyPetList" })
  );

  // Load the items from server
  const { loading, error, data } = useQuery(PET_LIST);

  const items = useMemo(() => deserialize(data?.petList), [data?.petList]);

  return (
    <div className="narrow-layout">
      <Space direction="vertical" className="list-space">
        <ListItems items={items} loading={loading} error={error} />
      </Space>
    </div>
  );
}

interface ListItemsProps {
  items?: ItemListType;
  loading?: boolean;
  error?: ApolloError;
}

/**
 * Collection of items
 */
function ListItems({ items, loading, error }: ListItemsProps) {
  if (loading) {
    return <Spin />;
  }

  if (error) {
    return <RequestFailedError />;
  }

  if (items == null || items.length === 0) {
    return <Empty />;
  }

  return (
    <Space direction="vertical" className="list-space">
      <List
        itemLayout="horizontal"
        bordered
        dataSource={items}
        renderItem={item => <ListItem item={item} key={item?.id} />}
      />
    </Space>
  );
}

function ListItem({ item }: { item: ItemType }) {
  if (item == null) {
    return null;
  }

  return (
    <List.Item>
      <div className="list-wrapper">
        <ValueWithLabel
          key="identificationNumber"
          label="Identification Number"
          value={item.identificationNumber ?? undefined}
        />
        <ValueWithLabel
          key="birthDate"
          label="Birth Date"
          value={item.birthDate?.format("LL") ?? undefined}
        />
        <ValueWithLabel
          key="type"
          label="Type"
          value={getPetTypeDtoDisplayName(item.type ?? undefined)}
        />
        <ValueWithLabel
          key="owner"
          label="Owner"
          value={getOwnerDtoDisplayName(item.owner ?? undefined)}
        />
        <ValueWithLabel
          key="description"
          label="Description"
          value={getPetDescriptionDtoDisplayName(item.description ?? undefined)}
        />
        <ValueWithLabel
          key="tags"
          label="Tags"
          value={
            item.tags &&
            item.tags
              .map(entry => getTagDtoDisplayName(entry))
              .filter(entry => entry !== "")
          }
        />
        <ValueWithLabel
          key="diseases"
          label="Diseases"
          value={
            item.diseases &&
            item.diseases
              .map(entry => getPetDiseaseDtoDisplayName(entry))
              .filter(entry => entry !== "")
          }
        />
      </div>
    </List.Item>
  );
}

/**
 * Type of data object received when executing the query
 */
type QueryResultType = ResultOf<typeof PET_LIST>;
/**
 * Type of the items list
 */
type ItemListType = QueryResultType["petList"];
/**
 * Type of a single item
 */
type ItemType = Exclude<ItemListType, null | undefined>[0];
